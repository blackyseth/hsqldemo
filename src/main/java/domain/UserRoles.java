package domain;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@javax.persistence.Entity
public class UserRoles extends Entity{
    @Id
    private int roleId;
    private String name;
    @OneToMany
    private List<RolesPermissions> roles;
    @OneToMany
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<RolesPermissions> getRoles() {
        return roles;
    }

    public void setRoles(List<RolesPermissions> roles) {
        this.roles = roles;
    }



}
