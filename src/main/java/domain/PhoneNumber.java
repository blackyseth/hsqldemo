package domain;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@javax.persistence.Entity
public class PhoneNumber extends Entity{
    @Id
    private int phoneNumberId;
    private int countryPrefix;
    private int cityPrefix;
    private long number;
    private int typeId;
    @OneToOne
    private Person person;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public int getCountryPrefix() {
        return countryPrefix;
    }

    public void setCountryPrefix(int countryPrefix) {
        this.countryPrefix = countryPrefix;
    }

    public int getCityPrefix() {
        return cityPrefix;
    }

    public void setCityPrefix(int cityPrefix) {
        this.cityPrefix = cityPrefix;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
}
