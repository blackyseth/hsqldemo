package domain;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

@javax.persistence.Entity
public class User extends Entity{
    @Id
    private int userId;
    private String login;
    private String password;
    @OneToMany
    private List<UserRoles> roles;
    @OneToOne
    private Person person;

    public User(){
        super();
        roles = new ArrayList<UserRoles>();
    }

    public List<UserRoles> getRoles() {
        return roles;
    }

    public void setRoles(UserRoles role) {
        roles.add(role);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
