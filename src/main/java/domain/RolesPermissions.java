package domain;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@javax.persistence.Entity
public class RolesPermissions extends Entity{
    private int roleId;
    @Id
    private int permissionId;
    @OneToMany
    private List<UserRoles> roles;

    public List<UserRoles> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRoles> roles) {
        this.roles = roles;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(int permissionId) {
        this.permissionId = permissionId;
    }
}
