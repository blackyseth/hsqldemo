
import domain.EnumerationValue;
import org.hibernate.Session;
import repo.IRepository;
import repo.RepositoryCatalog;
import UnitOfWork.IUnitOfWork;
import UnitOfWork.UnitOfWork;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class main {
    public static void main(String[] args) {


        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("t_sys");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Session session = (Session) entityManager.getDelegate();

        IUnitOfWork unitOfWork = new UnitOfWork(session);
        RepositoryCatalog repositoryCatalog = new RepositoryCatalog(unitOfWork);
        IRepository<EnumerationValue> repository = repositoryCatalog.enumerations();

        Cache cache = Cache.getInstance();
        cache.setRepositoryCatalog(repositoryCatalog);
        cache.getEnumerationNames();
        cache.checkNames();


        unitOfWork.commit();
        entityManager.close();
        entityManagerFactory.close();


    }
}
