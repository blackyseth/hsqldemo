import domain.Entity;
import domain.EnumerationValue;
import repo.IRepository;
import repo.RepositoryCatalog;

import java.util.*;

public class Cache{

    private Timer timer = new Timer();
    RepositoryCatalog repositoryCatalog;
    private Map<Entity,String> cachedObjects = new HashMap<>();
    private static Cache cache;
    private static Object token = new Object();

    TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            resetCache();
        }
    };

    private void setAutoRefreshingOfCache(){
        timer.schedule(timerTask,5000L);
    }

    private Cache(){
        setAutoRefreshingOfCache();
    }

    public static Cache getInstance(){

        if(cache == null){
            synchronized(token)
            {
                if(cache==null) {
                    cache = new Cache();
                }

            }
        }
        return cache;
    }

    public static void resetCache(){
        if(cache != null){
            synchronized (token){
                if (cache!=null){
                    cache = new Cache();
                }
            }
        }else {
            getInstance();
        }
    }

    public RepositoryCatalog getRepositoryCatalog() {
        return repositoryCatalog;
    }

    public void setRepositoryCatalog(RepositoryCatalog repositoryCatalog) {
        this.repositoryCatalog = repositoryCatalog;
    }

    public void getEnumerationNames(){
        System.out.println(repositoryCatalog);
       IRepository<EnumerationValue> repository = repositoryCatalog.enumerations();
       List<EnumerationValue> names = repository.getAll();
        for (EnumerationValue s : names) {
            cachedObjects.put(s,s.getEnumerationName());
        }
    }

    public void checkNames(){
        for (String s : cachedObjects.values()) {
            System.out.println(s);
        }

    }

    public void clearCache(){
        cachedObjects.clear();
    }
}