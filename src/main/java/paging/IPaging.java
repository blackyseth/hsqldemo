package paging;


import java.util.List;

public interface IPaging<T> {
    void calculatePages();
    List<T> getList();
    List<T> getListOnPage();
    int getPageSize();
    void setPageSize(int size);
    int getPage();
    void setPage(int page);
    int getPreviousPage();
    int getNextPage();
}
