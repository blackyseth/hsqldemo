package repo.hsql.interfaces;


import domain.Address;
import domain.Person;
import repo.IRepository;

public interface IAddressRepository extends IRepository<Address>{
    void withId(int id);
    void withCityAndStreet(String city, String street);
    void withPerson(Person person);
}
