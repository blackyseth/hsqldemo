package repo.hsql.interfaces;

import domain.EnumerationValue;
import repo.IRepository;

public interface IEnumerationValueRepository extends IRepository<EnumerationValue> {
    void withName(String name);
    void withIntKey(int key, String name);
    void withStringKey(String key, String name);
}
