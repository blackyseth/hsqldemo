package repo.hsql.interfaces;


import domain.Person;
import domain.User;
import repo.IRepository;

public interface IPersonRepository extends IRepository<Person> {
    void withId(int id);
    void withNameAndSurname(String name,String surname);
    void withPesel(long pesel);
    void withNip(long nip);
    void withEmail(String email);
    void withUser(User user);
}
