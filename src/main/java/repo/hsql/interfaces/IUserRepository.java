package repo.hsql.interfaces;

import domain.User;
import repo.IRepository;

public interface IUserRepository extends IRepository<User> {
    void withLogin(String login);
    void withLoginAndPassword(String login, String password);
    void setupRoles(User user);
}
