package repo.hsql.interfaces;


import domain.Person;
import domain.PhoneNumber;
import repo.IRepository;

public interface IPhoneNumberRepository extends IRepository<PhoneNumber>{
    void withId(int Id);
    void withNumber(long number);
    void withPerson(Person person);
}
