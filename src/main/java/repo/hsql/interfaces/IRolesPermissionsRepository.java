package repo.hsql.interfaces;


import domain.RolesPermissions;
import repo.IRepository;

public interface IRolesPermissionsRepository extends IRepository<RolesPermissions>{
    void withId(int id);
    void withPermissionId(int id);
}
