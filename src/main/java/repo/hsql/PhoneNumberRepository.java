package repo.hsql;


import UnitOfWork.IUnitOfWork;
import UnitOfWork.UnitOfWorkRepository;
import domain.Entity;
import domain.Person;
import domain.PhoneNumber;
import org.hibernate.Query;
import org.hibernate.Session;
import paging.PagingInfo;
import repo.hsql.interfaces.IPhoneNumberRepository;

import java.util.List;

public class PhoneNumberRepository implements IPhoneNumberRepository, UnitOfWorkRepository{

    private Session session;
    private IUnitOfWork unitOfWork;
    Query query;

    public PhoneNumberRepository(IUnitOfWork unitOfWork) {
        this.unitOfWork = unitOfWork;
        this.session = unitOfWork.getSession();
    }

    @Override
    public void withId(int Id) {
        String sql = "FROM PhoneNumber WHERE phoneNumberId = :id";
        query=session.createQuery(sql);
        query.setParameter("id",Id);
        System.out.println(query.list());
    }

    @Override
    public void withNumber(long number) {
        String sql = "FROM PhoneNumber WHERE number = :number";
        query=session.createQuery(sql);
        query.setParameter("number",number);
        System.out.println(query.list());
    }

    @Override
    public void withPerson(Person person) {
        String sql = "FROM PhoneNumber WHERE person = :person";
        query=session.createQuery(sql);
        query.setParameter("person",person);
        System.out.println(query.list());
    }

    public void allOnPage(PagingInfo page) {
        PagingInfo<PhoneNumber> pagingInfo = new PagingInfo<>(getAll());
    }

    public void add(PhoneNumber pn) {
        unitOfWork.markAsNew(pn,this);
    }

    public void delete(PhoneNumber pn) {
        unitOfWork.markAsDeleted(pn, this);
    }

    public void modify(PhoneNumber pn) {
        unitOfWork.markAsChanged(pn, this);
    }

    public int count() {
        return getAll().size();
    }


    public List<PhoneNumber> getAll() {
        String sql = "FROM PhoneNumber";
        query=session.createQuery(sql);
        return query.list();
    }



    public void persistAdd(Entity entity){
        session.persist(entity);
    }


    public void persistDelete(Entity entity) {
        session.delete(entity);
    }


    public void persistUpdate(Entity entity) {
        session.update(entity);
    }
}
