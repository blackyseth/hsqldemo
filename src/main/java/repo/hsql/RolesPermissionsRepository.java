package repo.hsql;


import UnitOfWork.IUnitOfWork;
import UnitOfWork.UnitOfWorkRepository;
import domain.Entity;
import domain.PhoneNumber;
import domain.RolesPermissions;
import org.hibernate.Query;
import org.hibernate.Session;
import paging.PagingInfo;
import repo.hsql.interfaces.IRolesPermissionsRepository;

import java.util.List;

public class RolesPermissionsRepository implements IRolesPermissionsRepository, UnitOfWorkRepository{
    private Session session;
    private IUnitOfWork unitOfWork;
    Query query;

    public RolesPermissionsRepository(IUnitOfWork unitOfWork) {
        this.unitOfWork = unitOfWork;
        this.session = unitOfWork.getSession();
    }

    @Override
    public void withId(int id) {
        String sql = "FROM RolesPermissions WHERE roleId = :id";
        query=session.createQuery(sql);
        query.setParameter("id",id);
        System.out.println(query.list());
    }

    @Override
    public void withPermissionId(int id) {
        String sql = "FROM RolesPermissions WHERE permissionId= :id";
        query=session.createQuery(sql);
        query.setParameter("id",id);
        System.out.println(query.list());
    }

    public void allOnPage(PagingInfo page) {
        PagingInfo<RolesPermissions> pagingInfo = new PagingInfo<>(getAll());
    }

    public void add(RolesPermissions rp) {
        unitOfWork.markAsNew(rp,this);
    }

    public void delete(RolesPermissions rp) {
        unitOfWork.markAsDeleted(rp, this);
    }

    public void modify(RolesPermissions rp) {
        unitOfWork.markAsChanged(rp, this);
    }

    public int count() {
        return getAll().size();
    }


    public List<RolesPermissions> getAll() {
        String sql = "FROM RolesPermissions";
        query=session.createQuery(sql);
        return query.list();
    }



    public void persistAdd(Entity entity){
        session.persist(entity);
    }


    public void persistDelete(Entity entity) {
        session.delete(entity);
    }


    public void persistUpdate(Entity entity) {
        session.update(entity);
    }
}
