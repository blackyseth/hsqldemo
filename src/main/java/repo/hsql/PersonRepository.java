package repo.hsql;


import UnitOfWork.IUnitOfWork;
import UnitOfWork.UnitOfWorkRepository;
import domain.Entity;
import domain.Person;
import domain.User;
import org.hibernate.Query;
import org.hibernate.Session;
import paging.PagingInfo;
import repo.hsql.interfaces.IPersonRepository;

import java.util.List;

public class PersonRepository implements IPersonRepository, UnitOfWorkRepository{

    private Session session;
    private IUnitOfWork unitOfWork;
    Query query;

    public PersonRepository(IUnitOfWork unitOfWork) {
        this.unitOfWork = unitOfWork;
        this.session = unitOfWork.getSession();
    }

    @Override
    public void withId(int id) {
        String sql = "FROM Person WHERE personId = :id";
        query=session.createQuery(sql);
        query.setParameter("id",id);
        System.out.println(query.list());
    }

    @Override
    public void withNameAndSurname(String name, String surname) {
        String sql = "FROM Person WHERE firstName = :name AND surname = :surname";
        query=session.createQuery(sql);
        query.setParameter("name",name);
        query.setParameter("surname",surname);
        System.out.println(query.list());
    }

    @Override
    public void withPesel(long pesel) {
        String sql = "FROM Person WHERE pesel = :pesel";
        query=session.createQuery(sql);
        query.setParameter("pesel",pesel);
        System.out.println(query.list());
    }

    @Override
    public void withNip(long nip) {
        String sql = "FROM Person WHERE nip = :nip";
        query=session.createQuery(sql);
        query.setParameter("nip",nip);
        System.out.println(query.list());
    }

    @Override
    public void withEmail(String email) {
        String sql = "FROM Person WHERE email = :email";
        query=session.createQuery(sql);
        query.setParameter("email",email);
        System.out.println(query.list());
    }

    @Override
    public void withUser(User user) {
        String sql = "FROM Person WHERE user = :user";
        query=session.createQuery(sql);
        query.setParameter("user",user);
        System.out.println(query.list());
    }

    public void allOnPage(PagingInfo page) {
        PagingInfo<Person> pagingInfo = new PagingInfo<>(getAll());
    }

    public void add(Person p) {
        unitOfWork.markAsNew(p,this);
    }

    public void delete(Person p) {
        unitOfWork.markAsDeleted(p, this);
    }

    public void modify(Person p) {
        unitOfWork.markAsChanged(p, this);
    }

    public int count() {
        return getAll().size();
    }


    public List<Person> getAll() {
        String sql = "FROM Person ";
        query=session.createQuery(sql);
        return query.list();
    }



    public void persistAdd(Entity entity){
        session.persist(entity);
    }


    public void persistDelete(Entity entity) {
        session.delete(entity);
    }


    public void persistUpdate(Entity entity) {
        session.update(entity);
    }
}
