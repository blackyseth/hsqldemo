package repo.hsql;
import org.hibernate.Query;
import org.hibernate.Session;
import paging.PagingInfo;
import UnitOfWork.IUnitOfWork;
import UnitOfWork.UnitOfWorkRepository;

import domain.Entity;
import domain.EnumerationValue;
import repo.hsql.interfaces.IEnumerationValueRepository;

import java.util.List;

public class EnumerationValuesRepository implements IEnumerationValueRepository, UnitOfWorkRepository{

    private Session session;
    private IUnitOfWork unitOfWork;
    Query query;

    public EnumerationValuesRepository(IUnitOfWork unitOfWork) {
        this.unitOfWork = unitOfWork;
        this.session = unitOfWork.getSession();
    }

    public void withName(String name) {
        String sql = "FROM EnumerationValue WHERE enumerationName = :name";
        query=session.createQuery(sql);
        query.setParameter("name",name);
        System.out.println(query.list());
    }

    public void withIntKey(int key, String name) {
        String sql = "FROM EnumerationValue WHERE enumerationName = :name AND intKey = :intkey";
        query=session.createQuery(sql);
        query.setParameter("name",name);
        query.setParameter("intkey",key);
        System.out.println(query.list());
    }

    public void withStringKey(String key, String name) {
        String sql = "FROM EnumerationValue WHERE enumerationName = :name AND stringKey = :intkey";
        query=session.createQuery(sql);
        query.setParameter("name",name);
        query.setParameter("intkey",key);
        System.out.println(query.list());
    }

    public void withId(int id) {
        String sql = "FROM EnumerationValue WHERE intKey = :id";
        query=session.createQuery(sql);
        query.setParameter("id",id);
        System.out.println(query.list());
    }

    public void allOnPage(PagingInfo page) {
        PagingInfo<EnumerationValue> pagingInfo = new PagingInfo<>(getAll());
    }

    public void add(EnumerationValue ev) {
        unitOfWork.markAsNew(ev,this);
    }

    public void delete(EnumerationValue ev) {
        unitOfWork.markAsDeleted(ev, this);
    }

    public void modify(EnumerationValue ev) {
        unitOfWork.markAsChanged(ev, this);
    }

    public int count() {
        return getAll().size();
    }


    public List<EnumerationValue> getAll() {
        String sql = "FROM EnumerationValue";
        query=session.createQuery(sql);
        return query.list();
    }



    public void persistAdd(Entity entity){
        session.persist(entity);
    }


    public void persistDelete(Entity entity) {
        session.delete(entity);
    }


    public void persistUpdate(Entity entity) {
        session.update(entity);
    }
}
