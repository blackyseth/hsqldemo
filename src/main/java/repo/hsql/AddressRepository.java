package repo.hsql;


import UnitOfWork.IUnitOfWork;
import UnitOfWork.UnitOfWorkRepository;
import domain.Address;
import domain.Entity;
import domain.EnumerationValue;
import domain.Person;
import org.hibernate.Query;
import org.hibernate.Session;
import paging.PagingInfo;
import repo.hsql.interfaces.IAddressRepository;

import java.util.List;

public class AddressRepository implements IAddressRepository, UnitOfWorkRepository{

    private Session session;
    private IUnitOfWork unitOfWork;
    Query query;

    public AddressRepository(IUnitOfWork unitOfWork) {
        this.unitOfWork = unitOfWork;
        this.session = unitOfWork.getSession();
    }

    @Override
    public void withId(int id) {
        String sql = "FROM Address WHERE addressId = :id";
        query=session.createQuery(sql);
        query.setParameter("id",id);
        System.out.println(query.list());
    }

    public void allOnPage(PagingInfo page) {
        PagingInfo<Address> pagingInfo = new PagingInfo<>(getAll());
    }

    public void add(Address address) {
        unitOfWork.markAsNew(address,this);
    }

    public void delete(Address address) {
        unitOfWork.markAsDeleted(address, this);
    }

    public void modify(Address address) {
        unitOfWork.markAsChanged(address, this);
    }


    public int count() {
        return getAll().size();
    }

    @Override
    public List<Address> getAll() {
        String sql = "FROM Address ";
        query=session.createQuery(sql);
        return query.list();
    }

    @Override
    public void withCityAndStreet(String city, String street) {
        String sql = "FROM Address WHERE city = :city AND street = :street";
        query=session.createQuery(sql);
        query.setParameter("city",city);
        query.setParameter("street",street);
        System.out.println(query.list());
    }

    @Override
    public void withPerson(Person person) {
        String sql = "FROM Address WHERE person = :person";
        query=session.createQuery(sql);
        query.setParameter("person",person);
        System.out.println(query.list());
    }


    public void persistAdd(Entity entity){
        session.persist(entity);
    }


    public void persistDelete(Entity entity) {
        session.delete(entity);
    }


    public void persistUpdate(Entity entity) {
        session.update(entity);
    }
}
