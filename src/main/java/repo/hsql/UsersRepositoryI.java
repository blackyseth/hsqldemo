package repo.hsql;

import domain.Entity;

import domain.UserRoles;
import org.hibernate.Query;
import org.hibernate.Session;
import paging.PagingInfo;
import UnitOfWork.IUnitOfWork;
import UnitOfWork.UnitOfWorkRepository;
import domain.User;
import repo.hsql.interfaces.IUserRepository;

import java.util.List;

public class UsersRepositoryI implements IUserRepository,UnitOfWorkRepository{

    private Session session;
    private IUnitOfWork unitOfWork;
    Query query;

    public UsersRepositoryI(IUnitOfWork unitOfWork) {
        this.unitOfWork = unitOfWork;
        this.session = unitOfWork.getSession();
    }

    @Override
    public void withLogin(String login) {
        String sql = "FROM User WHERE login = :login";
        query=session.createQuery(sql);
        query.setParameter("login",login);
        System.out.println(query.list());
    }

    @Override
    public void withLoginAndPassword(String login, String password) {
        String sql = "FROM User WHERE login = :login AND password = :pass";
        query=session.createQuery(sql);
        query.setParameter("pass",password);
        query.setParameter("login",login);
        System.out.println(query.list());
    }

    @Override
    public void setupRoles(User user) {
        String sql = "UPDATE User set roles =: roles WHERE User =:user";
        query=session.createQuery(sql);
        query.setParameter("roles",new UserRoles());
        query.setParameter("user",user);
        System.out.println(query.list());
    }

    public void withId(int id) {
        String sql = "FROM User WHERE userId = :id";
        query=session.createQuery(sql);
        query.setParameter("id",id);
        System.out.println(query.list());
    }

    public void allOnPage(PagingInfo page) {
        PagingInfo<User> pagingInfo = new PagingInfo<>(getAll());
    }

    public void add(User user) {
        unitOfWork.markAsNew(user,this);
    }

    public void delete(User user) {
        unitOfWork.markAsDeleted(user, this);
    }

    public void modify(User user) {
        unitOfWork.markAsChanged(user, this);
    }

    public int count() {
        return getAll().size();
    }


    public List<User> getAll() {
        String sql = "FROM User";
        query=session.createQuery(sql);
        return query.list();
    }



    public void persistAdd(Entity entity){
        session.persist(entity);
    }


    public void persistDelete(Entity entity) {
        session.delete(entity);
    }


    public void persistUpdate(Entity entity) {
        session.update(entity);
    }


}
