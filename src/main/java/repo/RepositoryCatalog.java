package repo;

import UnitOfWork.IUnitOfWork;
import repo.hsql.*;
import repo.hsql.interfaces.*;

public class RepositoryCatalog implements IRepositoryCatalog{

    private IUnitOfWork unitOfWork;

    public RepositoryCatalog(IUnitOfWork unitOfWork) {
        this.unitOfWork = unitOfWork;
    }

    public IEnumerationValueRepository enumerations() {
        return new EnumerationValuesRepository(unitOfWork);
    }

    public IUserRepository users() {
        return new UsersRepositoryI(unitOfWork);
    }

    public IAddressRepository addresses() {
        return new AddressRepository(unitOfWork);
    }

    public IPersonRepository persons() {
        return new PersonRepository(unitOfWork);
    }

    public IPhoneNumberRepository phoneNumbers() {
        return new PhoneNumberRepository(unitOfWork);
    }

    public IRolesPermissionsRepository roles() {
        return new RolesPermissionsRepository(unitOfWork);
    }

    @Override
    public void commit() {
        unitOfWork.commit();
    }
}
