package repo;

import repo.hsql.AddressRepository;
import repo.hsql.PersonRepository;
import repo.hsql.PhoneNumberRepository;
import repo.hsql.RolesPermissionsRepository;
import repo.hsql.interfaces.*;

public interface IRepositoryCatalog {
    IEnumerationValueRepository enumerations();
    IUserRepository users();
    IAddressRepository addresses();
    IPersonRepository persons();
    IPhoneNumberRepository phoneNumbers();
    IRolesPermissionsRepository roles();
    void commit();
}
