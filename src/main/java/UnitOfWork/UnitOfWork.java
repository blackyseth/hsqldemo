package UnitOfWork;


import domain.Entity;
import domain.EntityState;
import org.apache.commons.collections.map.HashedMap;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;

import java.util.HashMap;
import java.util.Map;

public class UnitOfWork implements  IUnitOfWork{

    private Session session;
    private Transaction transaction;
    private Map<Entity,UnitOfWorkRepository> entities = new HashMap<>();

    public UnitOfWork(Session session){
        this.session = session;
        this.transaction = session.getTransaction();
    }

    @Override
    public void commit() {
        transaction.begin();
        try {
            for (Entity entity : entities.keySet()) {
                switch (entity.getState()) {
                    case Modified: {
                        entities.get(entity).persistUpdate(entity);
                        break;
                    }
                    case Deleted: {
                        entities.get(entity).persistDelete(entity);
                        break;
                    }
                    case New: {
                        entities.get(entity).persistAdd(entity);
                        break;
                    }
                    case UnChanged:
                        break;
                    default:
                        break;
                }
            }
            transaction.commit();
            entities.clear();
        }catch (TransactionException e){
            e.printStackTrace();
            rollback();
        }
    }

    public Session getSession() {
        return this.session;
    }

    @Override
    public void rollback() {
        entities.clear();
        transaction.rollback();
    }

    @Override
    public void markAsNew(Entity entity, UnitOfWorkRepository unitOfWorkRepository) {
        entity.setState(EntityState.New);
        entities.put(entity,unitOfWorkRepository);
    }

    @Override
    public void markAsDeleted(Entity entity, UnitOfWorkRepository unitOfWorkRepository) {
        entity.setState(EntityState.Deleted);
        entities.put(entity,unitOfWorkRepository);
    }

    @Override
    public void markAsChanged(Entity entity, UnitOfWorkRepository unitOfWorkRepository) {
        entity.setState(EntityState.Modified);
        entities.put(entity,unitOfWorkRepository);
    }
}
