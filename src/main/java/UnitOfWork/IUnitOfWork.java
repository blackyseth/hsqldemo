package UnitOfWork;

import domain.Entity;
import org.hibernate.Session;

public interface IUnitOfWork {
    void commit();
    void rollback();
    void markAsNew(Entity entity,UnitOfWorkRepository unitOfWorkRepository);
    void markAsDeleted(Entity entity,UnitOfWorkRepository unitOfWorkRepository);
    void markAsChanged(Entity entity, UnitOfWorkRepository unitOfWorkRepository);
    Session getSession();
}
