package RuleChecker;

public interface ICanCheckRule<TEntity> {
    CheckResult checkRule(TEntity entity);
}
