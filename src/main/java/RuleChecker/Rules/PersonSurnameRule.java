package RuleChecker.Rules;


import RuleChecker.CheckResult;
import RuleChecker.ICanCheckRule;
import RuleChecker.RuleResult;
import domain.Person;

public class PersonSurnameRule implements ICanCheckRule<Person>{
    private CheckResult result = null;
    private String surname;

    public CheckResult checkRule(Person entity){
        this.surname = entity.getSurname();
        if(surname ==null||surname.equals("")){
            result = new CheckResult("Please insert name", RuleResult.Error);
        }else {
            result = new CheckResult("", RuleResult.Ok);
        }
        return result;
    }
}
