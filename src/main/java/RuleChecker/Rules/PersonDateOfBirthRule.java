package RuleChecker.Rules;

import RuleChecker.CheckResult;
import RuleChecker.ICanCheckRule;
import RuleChecker.RuleResult;
import domain.Person;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class PersonDateOfBirthRule implements ICanCheckRule<Person>{
    private Date dateOfBirth;
    private Boolean isDateValid = true;
    private Calendar dateOfBirthCalendar = new GregorianCalendar();
    private Calendar todayDate = Calendar.getInstance();

    @Override
    public CheckResult checkRule(Person person) {
        CheckResult result = null;
        this.dateOfBirth = person.getDateOfBirth();
        dateOfBirthCalendar.setTime(dateOfBirth);
        isTooYoung();
        isTooOld();
        wereYouBornToday();
        if(isDateValid){
            result = new CheckResult("", RuleResult.Ok);
        }else {
            result = new CheckResult("Give proper date", RuleResult.Error);
        }
        return result;
    }

    public void isTooYoung(){
        if((dateOfBirthCalendar.get(Calendar.YEAR)-todayDate.get(Calendar.YEAR))<13){
            isDateValid = false;
        }
    }

    public void isTooOld(){
        if((dateOfBirthCalendar.get(Calendar.YEAR)-todayDate.get(Calendar.YEAR))>122){
            isDateValid = false;
        }
    }

    public void wereYouBornToday(){
        if(dateOfBirth.equals(todayDate)){
            isDateValid = false;
        }
    }

    public Boolean getIsDateValid() {
        return isDateValid;
    }
}
