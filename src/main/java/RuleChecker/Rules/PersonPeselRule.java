package RuleChecker.Rules;

import RuleChecker.CheckResult;
import RuleChecker.ICanCheckRule;
import RuleChecker.RuleResult;
import domain.Person;

import java.util.Arrays;

public class PersonPeselRule implements ICanCheckRule<Person> {

    private long pesel;
    private int[] importanceOfPeselNumber = {1,3,7,9,1,3,7,9,1,3};
    private int[] peselNumbers;

    public String getPeselNumbers() {
        String peselnumbers = Arrays.toString(peselNumbers);
        peselnumbers = peselnumbers.replaceAll(", ", "").replace("[", "").replace("]", "");
        return peselnumbers;
    }

    @Override
    public CheckResult checkRule(Person person) {
        setPesel(person);
        return getCheckResult();
    }

    public void setPesel(Person person){
        this.pesel = person.getPesel();
        if(!isPeselLengthWrong()){
            getPeselAsArray();
        }
    }

    public CheckResult getCheckResult() {
        CheckResult result = null;
        if(isPeselLengthWrong()){
            result =  new CheckResult("Wrong length of pesel", RuleResult.Error);
        }else if(isControlNumberEqual()) {
            result = new CheckResult("", RuleResult.Ok);
        }
        return result;
    }

    public boolean isPeselLengthWrong() {
        return String.valueOf(pesel).length()!=11;
    }

    public boolean isControlNumberEqual() {
        return getPeselControlNumber() == this.peselNumbers[10];
    }

    public int getPeselControlNumber(){
        int addedNumbers=0;
        for(int i=0;i<peselNumbers.length-1;i++){
            addedNumbers += (peselNumbers[i]*importanceOfPeselNumber[i]);
        }
        addedNumbers = addedNumbers%10;
        if (addedNumbers == 0){
            addedNumbers = 0;
        }else {
            addedNumbers = 10 - addedNumbers;
        }
        return addedNumbers;
    }

    public void getPeselAsArray(){
        String peselAsString = String.valueOf(pesel);
        this.peselNumbers = new int[peselAsString.length()];
        for(int i=0;i<peselAsString.length();i++){
            this.peselNumbers[i] = peselAsString.charAt(i) - '0';
        }
    }
}
