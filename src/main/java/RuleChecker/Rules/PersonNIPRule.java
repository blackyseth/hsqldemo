package RuleChecker.Rules;


import RuleChecker.CheckResult;
import RuleChecker.ICanCheckRule;
import RuleChecker.RuleResult;
import domain.Person;

import java.util.Arrays;

public class PersonNIPRule implements ICanCheckRule<Person>{
    private long NIP;
    private int[] importanceOfNipNumber = {6,5,7,2,3,4,5,6,7};
    private int[] nipNumbers;

    public String getNIPNumbers() {
        String peselnumbers = Arrays.toString(nipNumbers);
        peselnumbers = peselnumbers.replaceAll(", ", "").replace("[", "").replace("]", "");
        return peselnumbers;
    }

    @Override
    public CheckResult checkRule(Person person) {
        setNIP(person);
        return getCheckResult();
    }

    public void setNIP(Person person){
        this.NIP = person.getNip();
    }

    public CheckResult getCheckResult() {
        CheckResult result = null;
        if(isNipLengthWrong()){
            result =  new CheckResult("Wrong length of NIP", RuleResult.Error);
        }else if(isControlNumberEqual()) {
            result = new CheckResult("", RuleResult.Ok);
        }
        return result;
    }

    public boolean isNipLengthWrong() {
        return String.valueOf(NIP).length()!=10;
    }

    public boolean isControlNumberEqual() {
        getNipAsArray();
        return getNipControlNumber() == this.nipNumbers[9];
    }

    public int getNipControlNumber(){
        int addedNumbers=0;
        for(int i=0;i<nipNumbers.length-1;i++){
            addedNumbers += (nipNumbers[i]* importanceOfNipNumber[i]);
        }
        addedNumbers = addedNumbers%11;
        if (addedNumbers == 0){
            addedNumbers = 0;
        }
        return addedNumbers;
    }

    public void getNipAsArray(){
        String peselAsString = String.valueOf(NIP);
        this.nipNumbers = new int[peselAsString.length()];
        for(int i=0;i<peselAsString.length();i++){
            this.nipNumbers[i] = peselAsString.charAt(i) - '0';
        }
    }

}
