package RuleChecker.Rules;

import RuleChecker.CheckResult;
import RuleChecker.ICanCheckRule;
import RuleChecker.RuleResult;
import domain.Person;

public class PersonFirstNameRule implements ICanCheckRule<Person> {
    private CheckResult result = null;
    private String name;

    public CheckResult checkRule(Person entity){
        this.name = entity.getFirstName();
        if(name==null){
            result = new CheckResult("Please insert name", RuleResult.Error);
        }else if(name.equals("")){
            result = new CheckResult("Please insert name",RuleResult.Error);
        }else {
            result = new CheckResult("", RuleResult.Ok);
        }
        return result;
    }
}
