package RuleChecker.Rules;

import RuleChecker.CheckResult;
import RuleChecker.ICanCheckRule;
import RuleChecker.RuleResult;
import domain.Person;

public class PersonEmailRule implements ICanCheckRule<Person>{
    private String email;
    private String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    private Boolean isFit;

    @Override
    public CheckResult checkRule(Person person) {
        this.email = person.getEmail();
        isFit = email.matches(EMAIL_REGEX);
        if(isFit){
            return new CheckResult("", RuleResult.Ok);
        }else {
            return new CheckResult("", RuleResult.Error);
        }
    }
}
