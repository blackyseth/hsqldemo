package RuleChecker.Rules;

import RuleChecker.RuleResult;
import domain.Person;
import org.junit.Test;


import static org.junit.Assert.*;

public class PersonEmailRuleTest {

    @Test
     public void testCheckRulePass() throws Exception {
        PersonEmailRule rule = new PersonEmailRule();
        Person p = new Person();
        p.setEmail("yolo@stupido.com");
        assertTrue(rule.checkRule(p).getResult()== RuleResult.Ok);
    }

    @Test
    public void testCheckRuleFail() throws Exception {
        PersonEmailRule rule = new PersonEmailRule();
        Person p = new Person();
        p.setEmail("dido.comgf");
        assertTrue(rule.checkRule(p).getResult()== RuleResult.Error);
    }
}