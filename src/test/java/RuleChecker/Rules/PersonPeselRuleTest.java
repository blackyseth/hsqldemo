package RuleChecker.Rules;

import RuleChecker.RuleResult;
import domain.Person;
import org.junit.Test;

import static org.junit.Assert.*;

public class PersonPeselRuleTest {

    @Test
    public void testForGettngPeselAsRightArray() throws Exception {
        PersonPeselRule rule = new PersonPeselRule();
        Person p = new Person();
        p.setPesel(44051401458L);
        rule.setPesel(p);
        assertEquals(String.valueOf(p.getPesel()),rule.getPeselNumbers());
    }

    @Test
    public void testPeselControlNumberCountingRight() throws Exception{
        PersonPeselRule rule = new PersonPeselRule();
        Person p = new Person();
        p.setPesel(44051401458L);
        rule.setPesel(p);
        assertEquals(8, rule.getPeselControlNumber());
    }

    @Test
    public void testPeselControlNumberCountingWrong() throws Exception{
        PersonPeselRule rule = new PersonPeselRule();
        Person p = new Person();
        p.setPesel(44051401455L);
        rule.setPesel(p);
        assertFalse(rule.isControlNumberEqual());
    }

    @Test
    public void testForPeselLength11Numbers() throws Exception {
        PersonPeselRule rule = new PersonPeselRule();
        Person p = new Person();
        p.setPesel(49040501580L);
        rule.checkRule(p);
        assertTrue(rule.getCheckResult().getResult() == RuleResult.Ok);
    }

    @Test
    public void testForPeselLengthLessThan11Numbers() throws Exception {
        PersonPeselRule rule = new PersonPeselRule();
        Person p = new Person();
        p.setPesel(10000000L);
        rule.checkRule(p);
        assertTrue(rule.getCheckResult().getResult() == RuleResult.Error);
    }

    @Test
    public void testForPeselLengthMoreThan11Numbers() throws Exception {
        PersonPeselRule rule = new PersonPeselRule();
        Person p = new Person();
        p.setPesel(10000000000000L);
        rule.checkRule(p);
        assertTrue(rule.getCheckResult().getResult() == RuleResult.Error);
    }
}