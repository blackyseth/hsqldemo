package RuleChecker.Rules;

import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import domain.Person;
import org.junit.Test;

import static org.junit.Assert.*;

public class PersonSurnameRuleTest {

    @Test
    public void testCheckFirstNameRule_For_NULL() throws Exception {
        Person p = new Person();
        PersonSurnameRule rule = new PersonSurnameRule();
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult()== RuleResult.Error);
    }
    @Test
    public void testCheckFirstNameRule_For_EMPTY() throws Exception {
        Person p = new Person();
        p.setSurname("");
        PersonSurnameRule rule = new PersonSurnameRule();
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult()==RuleResult.Error);
    }
    @Test
    public void testCheckFirstNameRule_For_CORRECT() throws Exception {
        Person p = new Person();
        p.setSurname("FromKongo");
        PersonSurnameRule rule = new PersonSurnameRule();
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult()==RuleResult.Ok);
    }
}