package RuleChecker.Rules;

import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import domain.Person;
import org.junit.Test;

import static org.junit.Assert.*;


public class PersonFirstNameRuleTest {

    @Test
    public void testCheckFirstNameRule_For_NULL() throws Exception {
        Person p = new Person();
        PersonFirstNameRule rule = new PersonFirstNameRule();
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult()== RuleResult.Error);
    }
    @Test
    public void testCheckFirstNameRule_For_EMPTY() throws Exception {
        Person p = new Person();
        p.setFirstName("");
        PersonFirstNameRule rule = new PersonFirstNameRule();
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult()==RuleResult.Error);
    }
    @Test
    public void testCheckFirstNameRule_For_CORRECT() throws Exception {
        Person p = new Person();
        p.setFirstName("Banana");
        PersonFirstNameRule rule = new PersonFirstNameRule();
        CheckResult result = rule.checkRule(p);
        assertTrue(result.getResult()==RuleResult.Ok);
    }
}