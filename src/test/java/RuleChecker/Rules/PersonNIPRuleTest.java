package RuleChecker.Rules;

import RuleChecker.RuleResult;
import domain.Person;
import org.junit.Test;

import static org.junit.Assert.*;


public class PersonNIPRuleTest {

    @Test
    public void testForGettngNipAsRightArray() throws Exception {
        PersonNIPRule rule = new PersonNIPRule();
        Person p = new Person();
        p.setNip(1234563218L);
        rule.setNIP(p);
        rule.getNipAsArray();
        assertEquals(String.valueOf(p.getNip()), rule.getNIPNumbers());
    }

    @Test
    public void testNipControlNumberCountingRight() throws Exception{
        PersonNIPRule rule = new PersonNIPRule();
        Person p = new Person();
        p.setNip(1234563218L);
        rule.setNIP(p);
        rule.getNipAsArray();
        assertEquals(8, rule.getNipControlNumber());
    }

    @Test
    public void testNipControlNumberCountingWrong() throws Exception{
        PersonNIPRule rule = new PersonNIPRule();
        Person p = new Person();
        p.setNip(1234563212L);
        rule.setNIP(p);
        assertFalse(rule.isControlNumberEqual());
    }

    @Test
    public void testForNipLength11Numbers() throws Exception {
        PersonNIPRule rule = new PersonNIPRule();
        Person p = new Person();
        p.setNip(1234563218L);
        rule.checkRule(p);
        assertTrue(rule.getCheckResult().getResult() == RuleResult.Ok);
    }

    @Test
    public void testForNipLengthLessThan11Numbers() throws Exception {
        PersonNIPRule rule = new PersonNIPRule();
        Person p = new Person();
        p.setNip(12318L);
        rule.checkRule(p);
        assertTrue(rule.getCheckResult().getResult() == RuleResult.Error);
    }

    @Test
    public void testForNipLengthMoreThan11Numbers() throws Exception {
        PersonNIPRule rule = new PersonNIPRule();
        Person p = new Person();
        p.setNip(12345633333333218L);
        rule.checkRule(p);
        assertTrue(rule.getCheckResult().getResult() == RuleResult.Error);
    }
}